/*
Iz zadate ulazne datoteke učitati niz struktura pri čemu se struktura naucnik_st sastoji od sledećih
polja:
ime (jedna reč, do 20 karaktera)
prezime (jedna reč, do 20 karaktera)
broj objavljenih radova (nenegativan ceo broj)
oblast u kojoj istražuje (jedna reč, do 30 karaktera)
Nakon toga:
a) U izlaznu datoteku ispisati podatke o naučnicima koji imaju broj objavljenih radova veći od
zadatog.
b) Na standardni izlaz ispisati prosečnu dužinu prezima uzimajući u obzir samo osobe sa
različitim prezimenima.
c) Za zadatu naučnu oblast izračunati ukupan broj objavljenih radova i taj podatak sačuvati u
fajlu koji nosi naziv te oblasti sa ekstenzijom .txt
Nazivi ulazne i izlazne datoteke kao i broj objavljenih radova se zadaju kao argumenti poziva
programa dok naziv oblasti korisnik zadaje tokom izvršavanja programa kroz standardni ulaz.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME 21
#define MAX_EL 50
#define MAX_OBLAST 31

typedef struct naucnik_st {
	char ime[MAX_NAME];
	char prezime[MAX_NAME];
	unsigned broj_radova;
	char oblast[MAX_OBLAST];
}NAUCNIK;

FILE *safe_open(char *, char*);
void ucitavanje(FILE*, NAUCNIK*, int*);
void ispis(NAUCNIK , FILE *);
void ispisi_sve(NAUCNIK *, int, FILE *);
void ispis_vecih_od_zadatog(FILE *, NAUCNIK *, int, int);
int jedinstveno_prezime(NAUCNIK *, int, int);
void prosecna_duzina(NAUCNIK *, int);
void br_radova_po_oblasti(NAUCNIK *, int, FILE *, char *);

int main(int brArgs, char*args[]) {

	if (brArgs != 4) {
		printf("Nije uneto dovoljno argumenata\n");
		exit(2);
	}
	
	FILE *in, *out, *foblast;
	
	in = safe_open(args[1], "r");
	out = safe_open(args[2], "w");
	
	NAUCNIK niz[MAX_EL];
	int n;
	unsigned br_rad = atoi(args[3]);
	
	ucitavanje(in, niz, &n);
	ispisi_sve(niz, n, stdout);
	
	
	ispis_vecih_od_zadatog(out, niz, n, br_rad);	// pod A
	prosecna_duzina(niz,n);		//pod B
	
	char odabrana_oblast[MAX_OBLAST];		
	char fajl_oblasti[MAX_OBLAST+4];
	printf("Unesite zeljenu oblast: ");
	scanf("%s", odabrana_oblast);
	

	strcpy(fajl_oblasti, odabrana_oblast);
	strcat(fajl_oblasti, ".txt");
	
	foblast = safe_open(fajl_oblasti, "w");
	br_radova_po_oblasti(niz, n, foblast, odabrana_oblast);	//pod C
	
	fclose(in);
	fclose(out);
	fclose(foblast);
	
	return 0;
}

FILE *safe_open(char *name, char *mode) {
	FILE *fp = fopen(name, mode);
	
	if (fp == NULL) {
		printf("Greska prilikom otvaranja %s datoteke.\n", name);
		exit(1);
	}
	
	return fp;
}
void ucitavanje(FILE *in, NAUCNIK *naucnici, int *pn) {
	*pn = 0;
	
	while(fscanf(in, "%s %s %u %s", naucnici[*pn].ime, naucnici[*pn].prezime, &naucnici[*pn].broj_radova, naucnici[*pn].oblast) != EOF) {
		(*pn)++;
	}
}

void ispisi_sve(NAUCNIK *niz, int n, FILE *out) {
	int i;
	for (i=0; i<n; i++) {
		ispis(niz[i], out);
	}
}
void ispis(NAUCNIK n, FILE *out) {
	fprintf(out, "%s %s %u %s\n", n.ime, n.prezime, n.broj_radova, n.oblast);
}

void ispis_vecih_od_zadatog(FILE *out, NAUCNIK *niz, int n, int granica) {
	int i;
	
	for(i=0; i<n; i++) {
		if (niz[i].broj_radova > granica) {
			ispis(niz[i], out);
		}
	}
}
int jedinstveno_prezime(NAUCNIK *niz, int n, int trenutni) {
	int i;
	
	for(i=trenutni+1; i<n; i++) {
		if (strcmp(niz[i].prezime, niz[trenutni].prezime) == 0) {
			return 0; 	//strcmp vraca nulu ukuliko su stringovi jednaki
		}
	}
	
	return 1;
}


void prosecna_duzina(NAUCNIK *niz, int n) {
	int i, ukupna_duzina =0, ukupan_broj = 0;
	for (i=0; i<n; i++) {
		if (jedinstveno_prezime(niz, n, i) == 1) {
			ukupna_duzina += strlen(niz[i].prezime);
			ukupan_broj++;
		}
	}
	
	printf("Prosecna duzina prezimena je %.2f.\n", (float)ukupna_duzina/ukupan_broj);
}

void br_radova_po_oblasti(NAUCNIK *niz, int n, FILE *out, char *oblast) {
	int i, ukupno =0;
	for (i=0; i<n; i++) {
		if (strcmp(niz[i].oblast, oblast) == 0) {
			ukupno += niz[i].broj_radova;
		}
	}
	
	fprintf(out, "%d", ukupno);
}

