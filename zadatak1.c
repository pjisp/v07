/*
Prebrojati re�i iz ulazne datoteke. U
izlaznoj datoteci ispisati broj re�i i
najdu�u prona�enu re�. Koristiti
funkcije, imena datoteka prihvatiti
kroz argumente komandne linije.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_NAME 30
#define MAX_STRING 101

FILE *safe_open(char*, char*);
void ucitavanje(FILE *, FILE *);

int main(int brArgs, char *args[])
{
    FILE *in, *out;
    char sin[MAX_NAME], sout[MAX_NAME];

	if (brArgs != 3) {
		printf("Nije dobro pozvan program. Poziv treba da izgleda ovako:\n\tIME_PROGRAMA NAZIV_ULAZNOG_FAJLA NAZIV_IZLAZNOG_FAJLA\n");
		exit(EXIT_FAILURE);
	}
	
    strcpy(sin, args[1]);
    strcpy(sout, args[2]);

    in = safe_open(sin, "r");
    out = safe_open(sout, "w");

    ucitavanje(in, out);

    fclose(in);
    fclose(out);

    return 0;
}

FILE *safe_open(char *name, char *mode) {
    FILE *fp = fopen(name, mode);

    if(fp == NULL) {
        printf("Doslo je do greske prilikom otvaranja fajla %s.\n", name);
        exit(EXIT_FAILURE);
    }

    return fp;
}
void ucitavanje(FILE *in, FILE *out) {
    int broj =0;
    char string[MAX_STRING];
    int max_s = 0;
    char longest[MAX_STRING];

    while(fscanf(in, "%s", string) != EOF) {
        broj++;
        if (max_s < strlen(string)) {
            max_s = strlen(string);
            strcpy(longest, string);
        }
    }

    fprintf(out,"Ucitano je %d stringova od kojih je najduzi: %s.\n", broj, longest);
}
